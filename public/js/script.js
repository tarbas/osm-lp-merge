
// dot navigation slider

$('.feedback-slider:not(:first)').hide();
$('.dot').click(function () {
    let wasActive = $('.active-dot').index();
    $('.active-dot').removeClass('active-dot');
    $('.feedback-slider').eq(wasActive).hide();
    $('.feedback-slider').eq($(this).index()).fadeIn(1000);
    $(this).addClass('active-dot')
});


$('.prev').click(previousSlide);
$('.next').click(nextSlide);

$('.partners-slider-images:not(:first)').hide();
function previousSlide() {
    let currentIndex = $('.active-nav-button').index();
    $('.partners-slider-images').eq(currentIndex).hide();
    $('.active-nav-button').removeClass('active-nav-button');
    $('.nav-button').eq(currentIndex - 1).addClass('active-nav-button');
    $('.active-slider-number').removeClass('active-slider-number');
    $('.img-slider-navigator-number').eq(currentIndex - 1).addClass('active-slider-number');
    $('.partners-slider-images').eq(currentIndex - 1).fadeIn(1000);
}

function nextSlide() {
    let currentIndex = $('.active-nav-button').index();
    $('.partners-slider-images').eq(currentIndex).hide();
    currentIndex = currentIndex === $('.nav-button').length - 1 ? -1 : $('.active-nav-button').index();
    $('.active-nav-button').removeClass('active-nav-button');
    $('.nav-button').eq(currentIndex + 1).addClass('active-nav-button');
    $('.active-slider-number').removeClass('active-slider-number');
    $('.img-slider-navigator-number').eq(currentIndex + 1).addClass('active-slider-number');
    $('.partners-slider-images').eq(currentIndex + 1).fadeIn(1000);
}


// кнопка прокрутки вверх

    $('#toTop').click(function () {

        $('body,html').animate({
            scrollTop: 0
        }, 800);

    });



// бэкграунд для хедера
$(function () {

    $(window).scroll(function () {

        if ($(this).scrollTop() > 50) {

            $('.main-page-header').css('background-color', 'rgba(255, 255, 255, 0.8)');
            $('#toTop').css('cursor','pointer');

        } else {

            $('.main-page-header').css('background-color', '');
            $('#toTop').css('cursor','');

        }

    });
});